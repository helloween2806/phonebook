<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registration</title>
</head>
<body>
<form method="post" action="registration.jsp">
    <center>
        <table border="1" width="30%" cellpadding="5">
            <thead>
            <tr>
                <th colspan="2">New Record</th>
            </tr>
            </thead>
            <tbody>

                <td>First Name</td>
                <td><input type="text" name="fname" value="" /></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lname" value="" /></td>
            </tr>
            <tr>
                <td>Patronymic</td>
                <td><input type="text" name="patronymic" value="" /></td>
            </tr>
            <tr>
                <td>Mobile Phone</td>
                <td><input type="text" name="mobilephone" value="" /></td>
            </tr>

            <tr>
                <td>Home Phone</td>
                <td><input type="text" name="homephone" value="" /></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><input type="text" name="address" value="" /></td>
            </tr>

            <tr>
                <td>E-mail</td>
                <td><input type="text" name="email" value="" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit" /></td>
                <td><input type="reset" value="Reset" /></td>
            </tr>
            </tbody>
        </table>
    </center>
</form>
</body>
</html>