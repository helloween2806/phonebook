<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registration</title>
</head>
<body>
<div align="right">
    <a href="/login">Sign In</a>
</div>
<form method="post" action="/newUser" commandName="newUser">
    <center>
        <table border="1" width="30%" cellpadding="5">
            <thead>
            <tr>
                <th colspan="2">Enter Information Here</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>First Name</td>
                <td><input type="text" name="firstName" value="" title="At least 5 symbols"/></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lastName" value="" title="At least 5 symbols"/></td>
            <tr>
                <td>Patronymic</td>
                <td><input type="text" name="patronymic" value="" title="At least 5 symbols"/></td>
            </tr>
            </tr>
            <td>User Name</td>
            <td><input type="text" name="username" value="" title="At least 3 symbols (a-z,A-Z)"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" value="" title="5-15 symbols"/></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="password" name="confirmPassword" value="" title="At least 5 symbols"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit"/></td>
                <td><input type="reset" value="Reset"/></td>
            </tr>
            </tbody>
        </table>
        <p>${message}</p>
    </center>
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
</body>
</html>