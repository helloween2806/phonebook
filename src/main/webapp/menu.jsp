<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
Created by IntelliJ IDEA.
User: robert
Date: 21.04.16
Time: 20:28
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Menu</title>
</head>
<body>
<form>
    <div align="right">
        <a href="<c:url value="/logout" />">Logout</a>
    </div>
    <p><a href="/record.jsp">Add Record</a></p>
    <p><a href="/allrecords">Records</a></p>
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
</body>
</html>
