<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">


<body>

<div>
    <form method='get' action="/filter">
        <div align="right">
            <a href="<c:url value="/logout" />">Logout</a>
        </div>
        First Name:<input type="text" name="firstName" title="Filter by first name">
        Last Name:<input type="text" name="lastName" title="Filter by last name">
        <input type="submit" value="Filter" title="Press to filter"/>
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <p>
    <form method="get" action="/filterByMobilePhone">
        Mobile Phone:<input type="text" name="mobilePhone" title="Filter by mobile phone">

        <input type="submit" value="Filter" title="Filter by mobile phone">
    </form>
    </p>
    <form method="get" action="/allrecords">
        <input type="submit" value="All records" title="Get all records">
    </form>
    <div align="right">
    </div>
    <h1>Records</h1>

    <table>
        <thead>
        <tr>
            <th>#ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Patronymic</th>
            <th>Mobile Phone</th>
            <th>Home Phone</th>
            <th>Address</th>
            <th>Email</th>
        </tr>
        </thead>

        <c:forEach var="record" items="${records}">
            <tr>
                <td>
                        ${record.id}
                </td>
                <td>${record.firstName}</td>
                <td>${record.lastName}</td>
                <td>${record.patronymic}</td>
                <td>${record.mobilePhone}</td>
                <td>${record.homePhone}</td>
                <td>${record.address}</td>
                <td>${record.email}</td>
                <td>
                    <spring:url value="/${record.id}/delete" var="deleteUrl"/>
                    <spring:url value="/${record.id}/update" var="updateUrl"/>
                    <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
                    <button class="btn btn-danger" onclick="location.href='${deleteUrl}'">Delete</button>
                </td>
            </tr>
        </c:forEach>
    </table>
    <p>${message}</p>
    <button onclick="location.href='/home'">Add record</button>
</div>


</body>
</html>
