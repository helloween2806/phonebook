<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>New Record</title>
</head>
<body>
<div align="right">
    <a href="<c:url value="/logout" />">Logout</a>
    <p><a href="/allrecords">Records</a></p>
</div>
<form method='POST' action="/home"  modelAttribute="userRecord">
    <center>
        <input type="text" name="id" hidden value="${userRecord.id}">
        <table border="1" width="30%" cellpadding="5">
            <thead>
            <tr>
                <th colspan="2">New Record</th>
            </tr>
            </thead>
            <tbody>

            <td>First Name</td>
            <td><input type="text" name="firstName" value="${userRecord.firstName}"/></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lastName" value="${userRecord.lastName}"/></td>
            </tr>
            <tr>
                <td>Patronymic</td>
                <td><input type="text" name="patronymic" value="${userRecord.patronymic}"/></td>
            </tr>
            <tr>
                <td>Mobile Phone</td>
                <td><input type="text" name="mobilePhone" value="${userRecord.mobilePhone}" title="(050) 121-34-57"/></td>
            </tr>

            <tr>
                <td>Home Phone</td>
                <td><input type="text" name="homePhone" value="${userRecord.homePhone}" title="(050) 121-34-57"/></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><input type="text" name="address" value="${userRecord.address}"/></td>
            </tr>

            <tr>
                <td>E-mail</td>
                <td><input type="text" name="email" value="${userRecord.email}"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit"/></td>
                <td><input type="reset" value="Reset"/></td>
            </tr>
            </tbody>
        </table>
        <p>${message}</p>
    </center>
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
</body>
</html>