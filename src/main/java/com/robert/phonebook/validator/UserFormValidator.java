package com.robert.phonebook.validator;

import com.robert.phonebook.model.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by robert on 26.04.16.
 */
public class UserFormValidator implements Validator {

    private Pattern pattern;
    private Matcher matcher;

    String STRING_PATTERN = "[a-zA-Zа-яА-Я]+";
    String USERNAME_STRING_PATTERN = "[a-zA-Z]+";


    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDetails userDetails = (UserDetails) target;

        if (userDetails.getFirstName().length() < 5) {
            errors.rejectValue("firstName", "firstName.exceed",
                    "First Name must contain more than 4 digits");
        }
        if (!(userDetails.getFirstName() != null && userDetails.getFirstName().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userDetails.getFirstName());
            if (!matcher.matches()) {
                errors.rejectValue("firstName", "name.containNonChar",
                        "Enter a valid first name");
            }
        }
        if (userDetails.getLastName().length() < 5) {
            errors.rejectValue("lastName", "lastName.exceed",
                    "Last Name must contain more than 4 digits");
        }
        if (!(userDetails.getLastName() != null && userDetails.getLastName().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userDetails.getLastName());
            if (!matcher.matches()) {
                errors.rejectValue("lastName", "name.containNonChar",
                        "Enter a valid last name");
            }
        }
        if (userDetails.getUsername().length() < 3) {
            errors.rejectValue("firstName", "id.exceed",
                    "Username must contain more than 4 digits");
        }
        if (!(userDetails.getUsername() != null && userDetails.getUsername().isEmpty())) {
            pattern = Pattern.compile(USERNAME_STRING_PATTERN);
            matcher = pattern.matcher(userDetails.getUsername());
            if (!matcher.matches()) {
                errors.rejectValue("username", "name.containNonChar",
                        "Enter a valid username");
            }
        }
        if (userDetails.getPatronymic().length() < 5) {
            errors.rejectValue("patronymic", "patronymic.exceed",
                    "Patronymic must contain more than 4 digits");
        }
        if (!(userDetails.getPatronymic() != null && userDetails.getPatronymic().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userDetails.getPatronymic());
            if (!matcher.matches()) {
                errors.rejectValue("patronymic", "name.containNonChar",
                        "Enter a valid patronymic");
            }
        }

    }

}
