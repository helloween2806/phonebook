package com.robert.phonebook.validator;

import com.robert.phonebook.model.UserRecord;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by robert on 26.04.16.
 */
public class RecordFormValidator implements Validator {

    private Pattern pattern;
    private Matcher matcher;

    private final String STRING_PATTERN = "[a-zA-Zа-яА-Я]+";
    private final String MOBILE_PHONE_PATTERN = "\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{2}-[0-9]{2}";


    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserRecord userRecord = (UserRecord) target;
        if (userRecord.getFirstName().length() < 5) {
            errors.rejectValue("firstName", "firstName.exceed",
                    "First Name must contain more than 4 digits");
        }
        if (!(userRecord.getFirstName() != null && userRecord.getFirstName().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userRecord.getFirstName());
            if (!matcher.matches()) {
                errors.rejectValue("firstName", "name.containNonChar",
                        "Enter a valid first name");
            }
        }
        if (userRecord.getLastName().length() < 5) {
            errors.rejectValue("lastName", "lastName.exceed",
                    "Last Name must contain more than 4 digits");
        }
        if (!(userRecord.getLastName() != null && userRecord.getLastName().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userRecord.getLastName());
            if (!matcher.matches()) {
                errors.rejectValue("lastName", "name.containNonChar",
                        "Enter a valid last name");
            }
        }
        if (userRecord.getPatronymic().length() < 5) {
            errors.rejectValue("patronymic", "patronymic.exceed",
                    "Patronymic must contain more than 4 digits");
        }
        if (!(userRecord.getPatronymic() != null && userRecord.getPatronymic().isEmpty())) {
            pattern = Pattern.compile(STRING_PATTERN);
            matcher = pattern.matcher(userRecord.getPatronymic());
            if (!matcher.matches()) {
                errors.rejectValue("patronymic", "name.containNonChar",
                        "Enter a valid patronymic");
            }
        }
        if (!(userRecord.getMobilePhone() != null && userRecord.getMobilePhone().isEmpty())) {
            pattern = Pattern.compile(MOBILE_PHONE_PATTERN);
            matcher = pattern.matcher(userRecord.getMobilePhone());
            if (!matcher.matches()) {
                errors.rejectValue("mobilePhone", "name.containNonChar",
                        "Enter a valid mobile phone, format:(050) 121-34-57");
            }
        }

        if (!(userRecord.getHomePhone() != null && userRecord.getHomePhone().isEmpty())) {
            pattern = Pattern.compile(MOBILE_PHONE_PATTERN);
            matcher = pattern.matcher(userRecord.getHomePhone());
            if (!matcher.matches()) {
                errors.rejectValue("homePhone", "name.containNonChar",
                        "Enter a valid mobile phone, format:(050) 121-34-57");
            }
        }
    }

}

