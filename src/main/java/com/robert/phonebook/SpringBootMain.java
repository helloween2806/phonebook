package com.robert.phonebook;

import com.robert.phonebook.model.UserDetails;
import com.robert.phonebook.model.UserRecord;
import com.robert.phonebook.repository.UserDetailsRepository;
import com.robert.phonebook.repository.UserRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * Created by robert on 16.04.16.
 */
@SpringBootApplication
public class SpringBootMain extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(SpringBootMain.class);

    @Bean
    public CommandLineRunner demo(UserRecordRepository repository) {
        return (args) -> {
        UserRecord userRecord=repository.findById(16l);

        };
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootMain.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(SpringBootMain.class, args);
    }


    @Bean
    public ApplicationSecurity applicationSecurity() {
        return new ApplicationSecurity();
    }


}
