package com.robert.phonebook.service;

import com.robert.phonebook.model.UserDetails;
import com.robert.phonebook.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by robert on 20.04.16.
 */
@Component
public class UserDetailsService {

    @Autowired
    UserDetailsRepository userDetailsRepository;

    public void saveUser(UserDetails userDetails) {
        userDetailsRepository.save(userDetails);
    }

    public List<UserDetails> findByUsername(String username) {
        return userDetailsRepository.findByUsername(username);
    }
}
