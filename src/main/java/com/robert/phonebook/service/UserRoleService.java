package com.robert.phonebook.service;

import com.robert.phonebook.repository.UserRoleRepository;
import com.robert.phonebook.model.UserRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by robert on 20.04.16.
 */
@Component
public class UserRoleService {

    @Autowired
    UserRoleRepository userRoleRepository;

    public void saveUserRole(UserRoles userRoles) {
        userRoleRepository.save(userRoles);
    }
}
