package com.robert.phonebook.service;

import com.robert.phonebook.model.UserRecord;
import com.robert.phonebook.repository.UserRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by robert on 19.04.16.
 */
@Component
public class RecordService {

    @Autowired
    UserRecordRepository userRecordRepository;

    public void saveRecord(UserRecord record) {
        UserRecord userRecord = userRecordRepository.findById(record.getId());
        if(userRecord==null) {
            userRecordRepository.save(record);
        }else{
            userRecord.setFirstName(record.getFirstName());
            userRecord.setLastName(record.getLastName());
            userRecord.setPatronymic(record.getPatronymic());
            userRecord.setAddress(record.getAddress());
            userRecord.setMobilePhone(record.getMobilePhone());
            userRecord.setHomePhone(record.getHomePhone());
            userRecord.setEmail(record.getEmail());
            userRecordRepository.save(userRecord);
        }
    }

    public List<UserRecord> findByUserId(Long userId) {
        return userRecordRepository.findByUserId(userId);
    }

    public List<UserRecord> findByFirstNameAndLastName(String firstName, String lastName, Long id) {
        return userRecordRepository.findByFirstNameAndLastNameAndUserId(firstName, lastName, id);
    }

    public List<UserRecord> findByFirstNameAndId(String firstName, Long id) {
        return userRecordRepository.findByFirstNameAndUserId(firstName, id);
    }

    public List<UserRecord> findByLastNameAndId(String lastName, Long id) {
        return userRecordRepository.findByLastNameAndUserId(lastName, id);
    }

    public List<UserRecord> findByMobilePhoneAndUserId(String mobilePhone, Long id) {
        return userRecordRepository.findByMobilePhoneAndUserId(mobilePhone, id);
    }

    public void deleteRecordById(Long id){
        userRecordRepository.deleteUserRecordById(id);
    }

    public UserRecord findRecordById(Long id){
        return userRecordRepository.findById(id);
    }

}
