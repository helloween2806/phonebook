package com.robert.phonebook.repository;


import com.robert.phonebook.model.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by robert on 16.04.16.
 */
@Repository
@Transactional
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
    List<UserDetails> findByUsername(String firstName);
}
