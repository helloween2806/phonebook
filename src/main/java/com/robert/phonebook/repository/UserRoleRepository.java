package com.robert.phonebook.repository;

import com.robert.phonebook.model.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by robert on 20.04.16.
 */
@Repository
@Transactional
public interface UserRoleRepository extends JpaRepository<UserRoles, Long> {
}
