package com.robert.phonebook.repository;

import com.robert.phonebook.model.UserRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by robert on 16.04.16.
 */
@Repository
@Transactional
public interface UserRecordRepository extends JpaRepository<UserRecord, Long> {

    List<UserRecord> findByUserId(Long userId);

    List<UserRecord> findByFirstNameAndUserId(String firstName, Long id);

    List<UserRecord> findByLastNameAndUserId(String lastName, Long id);

    List<UserRecord> findByFirstNameAndLastNameAndUserId(String firstName, String lastName, Long id);

    List<UserRecord> findByMobilePhoneAndUserId(String mobilePhone, Long id);

    UserRecord findById(Long id);


    @Modifying
    void deleteUserRecordById(Long id);
}
