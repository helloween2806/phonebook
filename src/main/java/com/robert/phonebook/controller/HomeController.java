package com.robert.phonebook.controller;


import com.robert.phonebook.model.*;
import com.robert.phonebook.service.RecordService;
import com.robert.phonebook.service.UserDetailsService;
import com.robert.phonebook.service.UserRoleService;
import com.robert.phonebook.validator.UserFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * Created by robert on 15.04.16.
 */
@Controller
public class HomeController {

    @Autowired
    RecordService recordService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    UserDetailsService userDetailsService;


    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String menu() {
        return "menu";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");
        return model;

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public ModelAndView getRegisterForm() {
        return new ModelAndView("registration", "newUser", new UserDetails());
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.POST)
    public String register(@ModelAttribute @Valid UserDetails userDetails, BindingResult bindingResult, RedirectAttributes redirectAttributes) throws IOException {
        String message;
        UserFormValidator formValidator = new UserFormValidator();

        formValidator.validate(userDetails, bindingResult);
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            message = "Registration failed. Please check the entered values :";
            for (ObjectError s : errors) {
                message += "<br>" + s.getDefaultMessage() + "; ";
            }
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/newUser";

        } else if (!(userDetails.getConfirmPassword().equals(userDetails.getPassword()))) {
            message = "Registration failed. Confirm your password properly.";
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/newUser";

        } else if (userDetailsService.findByUsername(userDetails.getUsername()).size() != 0) {
            message = "User with such username already exists";
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/newUser";
        } else {
            message = "You have successfully signed up";
            userDetails.setEnabled("true");
            UserRoles userRoles = new UserRoles();
            userRoles.setRole("USER");
            userRoles.setUsername(userDetails.getUsername());
            userDetailsService.saveUser(userDetails);
            userRoleService.saveUserRole(userRoles);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/newUser";
        }
    }
}

