package com.robert.phonebook.controller;

import com.robert.phonebook.model.UserDetails;
import com.robert.phonebook.model.UserRecord;
import com.robert.phonebook.service.RecordService;
import com.robert.phonebook.service.UserDetailsService;
import com.robert.phonebook.validator.RecordFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

@Controller
public class RecordController {

    @Autowired
    RecordService recordService;

    @Autowired
    UserDetailsService userDetailsService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home() {
        return new ModelAndView("record", "userRecord", new UserRecord());
    }

    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String addRecord(@ModelAttribute("userRecord") @Valid UserRecord userRecord, BindingResult bindingResult, RedirectAttributes redirectAttributes) throws IOException {
        String message;
        RecordFormValidator formValidator = new RecordFormValidator();
        formValidator.validate(userRecord, bindingResult);
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            message = "Failed to add record. Please check the entered values :";
            for (ObjectError s : errors) {
                message += "<br>" + s.getDefaultMessage() + "; ";
            }
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/home";
        } else if (userRecord.getId() == null) {
            message = "Record was added successfully";
        } else message = "Record was updated successfully";
        redirectAttributes.addFlashAttribute("message", message);
        String name = getCurrentUserName();
        List<UserDetails> user = userDetailsService.findByUsername(name);
        userRecord.setUserId(user.get(0).getId());
        recordService.saveRecord(userRecord);

        return "record";
    }


    private String getCurrentUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }


    @RequestMapping(value = "/allrecords", method = RequestMethod.GET)
    public String showAllRecords(Model model) {
        String name = getCurrentUserName();
        List<UserDetails> user = userDetailsService.findByUsername(name);
        Long userId = user.get(0).getId();
        model.addAttribute("records", recordService.findByUserId(userId));
        return "allrecords";

    }

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterRecords(HttpServletRequest httpServletRequest, Model model) throws IOException {
        String firstName = httpServletRequest.getParameter("firstName");
        String lastName = httpServletRequest.getParameter("lastName");
        String name = getCurrentUserName();
        List<UserDetails> user = userDetailsService.findByUsername(name);
        Long userId = user.get(0).getId();
        List<UserRecord> userRecords;

        if (firstName != "" && lastName != "") {
            userRecords = recordService.findByFirstNameAndLastName(firstName, lastName, userId);
            model.addAttribute("records", userRecords);
        } else if ((firstName == "") && (lastName != "")) {
            userRecords = recordService.findByLastNameAndId(lastName, userId);
            model.addAttribute("records", userRecords);
        } else if ((firstName != "") && (lastName == "")) {
            userRecords = recordService.findByFirstNameAndId(firstName, userId);
            model.addAttribute("records", userRecords);
        }

        return "allrecords";
    }

    @RequestMapping(value = "/filterByMobilePhone")
    public String filterByMobilePhone(HttpServletRequest httpServletRequest, Model model) throws IOException {
        String mobilePhone = httpServletRequest.getParameter("mobilePhone");
        List<UserRecord> userRecords;
        String name = getCurrentUserName();
        List<UserDetails> user = userDetailsService.findByUsername(name);
        Long userId = user.get(0).getId();
        if (mobilePhone != "") {
            userRecords = recordService.findByMobilePhoneAndUserId(mobilePhone, userId);
            model.addAttribute("records", userRecords);
        }

        return "allrecords";
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
    public String showUpdateUserForm(@PathVariable("id") Long id, Model model) {

        UserRecord userRecord = recordService.findRecordById(id);
        model.addAttribute("userRecord", userRecord);

        return "record";

    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String deleteRecord(@PathVariable("id") Long id, final RedirectAttributes redirectAttributes) {
        UserRecord userRecord = recordService.findRecordById(id);
        String firstName = userRecord.getFirstName();
        String lastName = userRecord.getLastName();

        recordService.deleteRecordById(id);

        String message = "Record " + "(" + firstName + " " + lastName + ") is deleted";
        redirectAttributes.addFlashAttribute("message", message);
        return "redirect:/allrecords";

    }

}